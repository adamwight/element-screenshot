// TODO:
//  * Save metadata and errors to a log

// TODO: how to use ES6 import for this?
const sync = require('@wdio/sync').default;

import Api from 'nodemw';
import chromedriver from 'chromedriver';
import fs from 'fs';
import {remote} from 'webdriverio';
import log from 'winston';

import config from '../config';

// Queue for single-threading the screenshots.
const screenshotQueue = [];

let browser: WebdriverIO.Browser = null;

/**
 * @returns {Object}
 *   Map of wiki database name to a list of unprefixed titles in the Template namespace.
 */
function loadTitleSamples() {
	return JSON.parse(
		fs.readFileSync(
			config.templateTitleSamplePath,
			'utf8'));
}

/**
 * Follow page links to find an example page where the given template has been transcluded.
 *
 * @param wiki {String} Database name for wiki, "enwiki"
 * @param templates {String[]} Array of unprefixed template titles.
 * @param callback {{Object: void}} Sink is called for each page discovered.
 */
function findExamplePages(wiki, templates, callback) {
	const host = config.wikiSiteMap[wiki];
	const client = new Api({
		protocol: 'https',
		server: host,
		path: '/w',
		debug: false,
		userAgent: 'WMDE Technical Wishes screenshots <techwish@wikimedia.de>',
	});
	for (const template of templates) {
		// TODO: restrict namespaces?
		client.api.call(
			{
				action: 'query',
				prop: 'transcludedin',
				// TODO: could request in batches
				titles: `Template:${template}`,
				tiprop: 'title',
				// Exclude transclusions from the Template and Template_talk namespace.
				// TODO: Fall back and try these if no other pages are found.
				tinamespace: [
					0, 1, 2, 3, 4, 5, 6, 7, 8, 9, /*10, 11,*/ 12, 13, 14, 15, 100,
					101, 108, 109, 118, 119, 446, 447, 710, 711, 828, 829, 2300,
					2301, 2302, 2303].join('|'),
				format: 'json',
				formatversion: 2,
			},
			(_err, _info, _next, data) => {
				const page = data.query.pages?.[0].transcludedin?.[0].title;
				if (page) {
					log.info(`Found Template:${template} transcluded on page ${page}`);
					callback({host: host, page: page, template: template});
				} else {
					log.warn(`Failed to retrieve any transclusions of template ${template}`);
				}
			}
		);
	}
}

function circumscribeGroup(elements: WebdriverIO.ElementArray) {
    if (!elements.length) {
		return null;
	}

	return browser.execute((elements, marginPx) => {
		// Find the smallest bounding rectangle containing all elements.
		var minX, maxX, minY, maxY;
		elements.forEach(el => {
			var elRect = el.getBoundingClientRect();
			if (!minX || elRect.left < minX) minX = elRect.left;
			if (!maxX || elRect.right > maxX) maxX = elRect.right;
			if (!minY || elRect.top < minY) minY = elRect.top;
			if (!maxY || elRect.bottom > maxY) maxY = elRect.bottom;
		});
		const top = Math.max(0, minY + window.scrollY - marginPx),
			left = Math.max(0, minX + window.scrollX - marginPx),
			width = maxX - minX + 2 * marginPx,
			height = maxY - minY + 2 * marginPx;

		var overlay = document.createElement('div');
		overlay.setAttribute(
			'style',
			`
				position: absolute;
				z-index: 1;
				top: ${top}px;
				left: ${left}px;
				width: ${width}px;
				height: ${height}px;
			`);
		document.body.appendChild(overlay);
		return overlay;
	}, elements, config.cropMarginPx);
}

/**
 * @return {boolean} True if the titles are identical after normalization.
 */
function compareTitles(titleA: string, titleB: string): boolean {
	return (typeof(titleA) === 'string' && typeof(titleB) === 'string')
		&& (titleA.replace(/_/g, ' ') === titleB.replace(/_/g, ' '));
}

/**
 * Find all group members with matching `about` attribute.
 */
function findGroupSiblings(element: WebdriverIO.Element): WebdriverIO.ElementArray {
	const groupId = element.getAttribute('about');
	return browser.$$(`[about="${groupId}"]`);
}

/**
 * Search the current browser page for an element created by transcluding `template`.
 *
 * The `data-mw` structure only includes properties for top-level templates, this function won't match templates used in another template's body,
 *
 * TODO:
 *   - Optimize to one or two DOM accesses.
 *   - Capture params and annotate the outputs.
 *
 * @param template {String} Unprefixed database name for template, like "Infobox_person".
 * @returns {Object | null} Browser element JSON corresponding to the transclusion, or `null` if none found.
 */
function findTemplateUsage(template) {
    const transclusionElement =
		browser.$$('[data-mw]')
		?.find(element =>
			JSON.parse(
				element.getAttribute('data-mw')
			).parts?.some(part => {
				const partTemplate = part.template?.target.href
					// Remove path and namespace, leaving the unprefixed title.
					?.replace(/^[^:]+:(.*)$/, '$1');

				return compareTitles(template, partTemplate);
			})
		);

	if (!transclusionElement) {
		return null;
	}

	const siblingElements = findGroupSiblings(transclusionElement);
	return circumscribeGroup(siblingElements);
}

/**
 *
 * @param host {String} Fully qualified domain name for the wiki.
 * @param page {String} Page title where the template is used, in readable form.
 * @param template {String} Template name
 */
function screenshotUsage(host, page, template) {
	// TODO: normalize with library
	const title = encodeURIComponent(page.replace(' ', '_'));
	// TODO: Calculate URL in a PageObject
	const url = `https://${host}/api/rest_v1/page/html/${title}`;
	log.info(`Fetching page markup from ${url}`);
	browser.url(url);

	// Sanity check and delay.
	browser.$('body.mw-body-content').waitForExist({timeout: 10000});

	const element = findTemplateUsage(template);
	if (!element) {
		log.warn(`Failed to find ${template} on page ${url}`);
		return;
	}

	const safeTitle = template.replace(/\//g, '%2F'),
		hostDir = `${config.outputDir}/${host}`,
		path = `${hostDir}/${safeTitle}.png`;

	// TODO: deduplicate
	fs.mkdirSync(hostDir, {recursive: true});

	saveElementScreenshot(browser.$(element), path, template);
}

/**
 * Take a screenshot and save to a given path.
 *
 * @param element {WebdriverIO.Element} WebdriverIO page element where the template was rendered.
 * @param path {String} Path to the new image file.
 * @param template {String} Template name.
 */
function saveElementScreenshot(element, path, template) {
	try {
	    element.saveScreenshot(path);
	} catch (e) {
		log.warn(`Exception while taking screenshot for template ${template}`, e.message);
	}
}

function startLogging() {
    // Note, this depends on the specific log implementation.
	log.level = 'info';
	log.add(
		log.transports.File,
		{filename: 'logs/main.log', json: false});
}

/**
 * Start ChromeDriver and the WebdriverIO client
 *
 * @param executor function to wrap in `sync`
 * @return {WebdriverIO.BrowserObject} synchronous client
 */
function startWebdriver(executor: () => void) {
	return chromedriver.start(
		[
			'--url-base=/wd/hub',
			'--port=4444',
			'--headless', // FIXME: doesn't seem to have an effect
			// '--log-level=DEBUG',
		],
		true // returnPromise
	).then(() => remote({
		// debug: true,
		logLevel: 'warn',
		path: '/wd/hub',
		port: 4444,
		capabilities: {
			browserName: 'chrome',
			'goog:chromeOptions': {
				args: [ '--headless' ],
			},
		},
	}))
	// Set up global
	.then(client => browser = client)
	// Wrap all following stages in wdio `sync`.  FIXME: Can this be done with a Promise?
	.then(() => sync(executor));
}

function main() {
	startLogging();
	startWebdriver(() => {
		log.info("Starting screenshot run...");
		const titleSamples = loadTitleSamples();

		for (const [wiki, templates] of Object.entries(titleSamples)) {
			findExamplePages(
				wiki,
				templates,
				params => screenshotQueue.push(params)
			);
		}

		// FIXME: non-halting
		// TODO: fan-out and merge both stages.
		while (true) {
			let params = null;
			while (params = screenshotQueue.shift()) {
			    // TODO: hide structure
				screenshotUsage(params.host, params.page, params.template)
			}
			browser.pause(1000);
		}
	});
}

main();
