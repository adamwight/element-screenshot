export default {
    templateTitleSamplePath: './template_samples.json',
    outputDir: './images',
    cropMarginPx: 10,

    // TODO: lookup service
    wikiSiteMap: {
        dewiki: 'de.wikipedia.org',
        enwiki: 'en.wikipedia.org',
        fawiki: 'fa.wikipedia.org',
        trwiki: 'tr.wikipedia.org',
    },
}