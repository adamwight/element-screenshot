Script to screenshot template usages

## Installation

    npm install

If you need a different version of chromedriver,
  CHROMEDRIVER_VERSION=83.0.1 npm install chromedriver --only=dev

## Usage

    npm run screenshot

The input file `template_samples.json` lists which wikis and templates to search.

Outputs can be found in the `images/` directory.
